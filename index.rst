.. sf_shaders documentation master file, created by
   sphinx-quickstart on Thu Jun  8 16:01:00 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sf_shaders's documentation!
======================================


.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:

    /src/surface/index.rst
    /src/utils/index.rst
