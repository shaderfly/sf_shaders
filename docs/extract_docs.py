import os
import re
import sys
from pprint import pprint


def write_tmp_rst(filename):
    fin = open(filename, 'r')
    code = fin.read()
    regex = ur"(/\*!((.*\r|.*\n)+)\*/|//!(.*))"
    matches = re.finditer(regex, unicode(code, "utf-8"))

    # print enumerate(matches)
    rst_file = filename.replace('.osl', '.rst')
    fout = open(rst_file, 'w')

    for matchNum, match in enumerate(matches):
        if match.group(2):
            fout.write(match.group(2))
        if match.group(4):
            foutwrite(match.group(4))

    fout.close()


def write_tmp_rst2(filename):
    fin = open(filename, 'r')
    raw_code = fin.readlines()
    in_comment = False
    comments = []
    code = []
    for line in raw_code:
        # sys.stdout.write(line)
        # if the line contains with /*!, then we are in_comment
        # therefore we account for two situations. One where the
        # closing comment is in a separate line and another where
        # the closing is in the same line
        open_matched = re.match(r'.*/\*!(.*)', line)
        if open_matched:
            # is there a closing tag in the line? if so, extract with a regex
            line_matched = re.match(r'.*/\*!(.*)\*/', line)
            if line_matched:
                comments.append(line_matched.group(1) + "\n\n")
                continue
            else:
                comments.append(open_matched.group(1))
                in_comment = True
                continue
        if in_comment:
            matched = re.match(r'(.*)\*/', line)
            if matched:
                comments.append(matched.group(1) )
                in_comment = False
                continue
            else:
                if open_matched:
                    comments.append(open_matched.group(1))
                else:
                    comments.append(line)
                continue
        # if the line contains // then we capture unti the end of the
        # line
        single_open_matched = re.match(r'.*//!(.*)', line)
        if single_open_matched:
            comments.append(single_open_matched.group(1) + "\n\n")
            continue

        # otherwise, we are just catpuring code
        code.append(line)

    rst_file = filename.replace('.osl', '.rst')
    fout = open(rst_file, 'w')
    fout.write("".join(comments))
    fout.write(".. highlight:: c\n")
    fout.write(".. code-block:: c\n")
    fout.write("  ".join(code))
    fout.close()


osl_files = []
# gather all the source files in the current directory
for root, dirs, files in os.walk('src'):
    for _thefile in files:
        if _thefile[-4:] == '.osl':
            osl_files.append(os.path.join(root,_thefile))


# for each file, extract the special comments
for _theOslFile in osl_files:
    comments = write_tmp_rst2(_theOslFile)

