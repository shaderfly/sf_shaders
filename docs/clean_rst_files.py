import os


rst_files = []
# gather all the source files in the current directory
for root, dirs, files in os.walk('src'):
    for _thefile in files:
        if _thefile[-4:] == '.rst' and _thefile != "index.rst":
            rst_files.append(os.path.join(root, _thefile))


# for each file, extract the special comments
for _theRstFile in rst_files:
    os.remove(_theRstFile)
