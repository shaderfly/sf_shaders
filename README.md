# ShaderFly Shader Library

The shaderfly shader library is a general purpose shading library using the OSL shading language. We decided to use OSL as our language as it is widely being adopted by many renderers. For more information on OSL, please read the [OSL README](https://github.com/imageworks/OpenShadingLanguage)

## Suported Renderers
At the moment the main supported renderer is [Arnold](https://www.solidangle.com/arnold/). Pixar's PRMan will also be supported with due time. Other render engines such as Blender's Cycles, Clarisse and Chaos Group's VRay, should all be able to support these shaders, however we do not expect testing against these renderers as part of the core development. We encourage the shading community to contribute in any porting that might be necessary for the shaders to work any other renderers which support OSL.

## Building The Library
We have put a lot of effort into making the library as easy to compile as possible. In order to be able to compile this library, please make sure that you have the following applications installed in your system.

* OSL Compiler, available at https://github.com/imageworks/OpenShadingLanguage. A pre-compiled version of OSL is included with **Arnold** and **PRMan**
* Arnold Renderer, available at https://www.solidangle.com/arnold/ . As previously explained, this is the only renderer supported by the library at the moment.
* cmake, available at https://cmake.org/


To build the library:

1. Go to the root directory of the shader library and type " *cmake* . ". This will build a lot of temporary intermediate files that the build system needs in order to compile the library
2. From the same directory type " *make* ". This will compile all of the shaders to the directory build/VERSION/
3. After building, you can type " *make install* " to install the shaders in their final "installed" location

## Building the documentation
Since one of the main goals of this project is education, we have included an sphynx based HTML documentation generator. In order to build the documents please run " *make docs* " from the root of the shader library, after you have ran cmake .


## Special Thanks and Inspiration
The main inspiration for creating this shader library is the work done by [Anders Langlands](http://anderslanglands.com/alshaders/index.html) and the release of the alshaders library. The main reasons for us creating a new library and not extending the alshaders are:

* The ShaderFly library will be geared towards learning. Meaning that we want to ensure that the shader source code is heavily documented, with back-references to the original source for any code or concept.
* The ShaderFly library will be 100% OSL. This will improve portability and allow for a larger adoption of the library.

The library will contain revisited code from many different sources such as the old ***Advanced RenderMan*** book, as well Siggraph papers and presentation. Here is a list of shader developers who have contributed to this project.

* Rudy Cortes - rudy@shaderfly.com
